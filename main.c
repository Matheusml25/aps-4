#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define clear() printf("\033[H\033[J")

int numeroRegistros(FILE *arq);

struct data {
    unsigned short int dia;
    unsigned short int mes;
    unsigned short int ano;
};

struct horario {
    unsigned short int hora;
    unsigned short int minuto;
    unsigned short int segundo;
};

struct registro {
    char nome[100];
    unsigned short int tamanho;
    struct data data;
    struct horario horario;
    float localizacao[2];
};

void main(int argc, char **argv) {
    FILE *arquivo = fopen(argv[1], "r");
    int count = numeroRegistros(arquivo);
    struct registro dados[count];
    lerDados(arquivo, dados);
    clear();
    printf("LENDO DADOS...");
    clear();
    showMenuGeral(dados, count);
}

int numeroRegistros(FILE *arq) {
    int numReg = 1;
    char ch;

    while ((ch = fgetc(arq)) != EOF) {
        if (ch == '\n') {
            numReg += 1;
        }
    }
    rewind(arq);

    return numReg;
}

void bubbleSortTamanho(struct registro *V, int size) {
    int i, terminado, ultima = size - 1;
    struct registro aux;

    do {
        terminado = 0;
        for (i = 0; i < ultima; i++) {
            if (V[i].tamanho > V[i + 1].tamanho) {
                aux = V[i];
                V[i] = V[i + 1];
                V[i+1] = aux;
                terminado = i;
            }
        }
        ultima -= 1;
    } while (terminado != 0);
}

void bubbleSortNome(struct registro *V, int size) {
    int i = 0, terminado = 0, ultima = size - 1;
    struct registro aux;

    do {
        terminado = 0;
        for (i = 0; i < ultima; i++) {
            if (V[i].nome[0] > V[i + 1].nome[0]) {
                aux = V[i];
                V[i] = V[i + 1];
                V[i + 1] = aux;
                terminado = i;
            }
        }
        ultima -= 1;
    } while (terminado != 0);
}

int compareData(struct data d1, struct data d2) { 
    if (d1.ano > d2.ano) 
        return 1; 
    if (d1.ano == d2.ano && d1.mes > d2.mes) 
        return 1; 
    if (d1.ano == d2.ano && d1.mes == d2.mes && 
                              d1.dia > d2.dia) 
        return 1; 
  
    return 0; 
}

int compareDataMenorIgual(struct data d1, struct data d2) { 
    if (d1.ano <= d2.ano) 
        return 1; 
    if (d1.ano == d2.ano && d1.mes <= d2.mes) 
        return 1; 
    if (d1.ano == d2.ano && d1.mes == d2.mes && 
                              d1.dia <= d2.dia) 
        return 1; 
  
    return 0; 
}

int compareDataMenor(struct data d1, struct data d2) { 
    if (d1.ano < d2.ano) 
        return 1; 
    if (d1.ano == d2.ano && d1.mes < d2.mes) 
        return 1; 
    if (d1.ano == d2.ano && d1.mes == d2.mes && 
                              d1.dia < d2.dia) 
        return 1; 
  
    return 0; 
}

void bubbleSortData(struct registro *V, int size) {
    int i = 0, terminado = 0, ultima = size - 1;
    struct registro aux;

    do {
        terminado = 0;
        for (i = 0; i < ultima; i++) {
            if (compareData(V[i].data, V[i + 1].data)) {
                aux = V[i];
                V[i] = V[i + 1];
                V[i + 1] = aux;
                terminado = i;
            }
        }
        ultima -= 1;
    } while (terminado != 0);
}

void quickSortTamanho(struct registro *V, int inicio, int fim) {
    int pivo = 0;
    if (fim > inicio) {
        pivo = particionaTamanho(V, inicio, fim);
        quickSortTamanho(V, inicio, pivo - 1);
        quickSortTamanho(V, pivo + 1, fim);
    }
}

void quickSortNome(struct registro *V, int inicio, int fim) {
    int pivo = 0;
    if (fim > inicio) {
        pivo = particionaNome(V, inicio, fim);
        quickSortNome(V, inicio, pivo - 1);
        quickSortNome(V, pivo + 1, fim);
    }
}

void quickSortData(struct registro *V, int inicio, int fim) {
    int pivo = 0;

    if (fim > inicio) {
        pivo = particionaData(V, inicio, fim);
        quickSortData(V, inicio, pivo - 1);
        quickSortData(V, pivo + 1, fim);
    }
}

int particionaData(struct registro *V, int inicio, int fim) {
    int esq = 0, dir = 0;
    struct registro aux, pivo;
    esq = inicio;
    dir = fim;
    pivo = V[inicio];

    while (esq < dir) {
        while (compareDataMenorIgual(V[esq].data, pivo.data))
            esq += 1;
        while (compareData(V[dir].data, pivo.data))
            dir -= 1;
        if (esq < dir) {
            aux = V[esq];
            V[esq] = V[dir];
            V[dir] = aux;
        }
    }

    V[inicio] = V[dir];
    V[dir] = pivo;
    return dir;
}

int particionaTamanho(struct registro *V, int inicio, int fim) {
    int esq = 0, dir = 0;
    struct registro aux, pivo;
    esq = inicio;
    dir = fim;
    pivo = V[inicio];

    while (esq < dir) {
        while (V[esq].tamanho <= pivo.tamanho) 
            esq += 1;
        while (V[dir].tamanho > pivo.tamanho)
            dir -= 1;
        if (esq < dir) {
            aux = V[esq];
            V[esq] = V[dir];
            V[dir] = aux;
        }
    }

    V[inicio] = V[dir];
    V[dir] = pivo;
    return dir;
}

int particionaNome(struct registro *V, int inicio, int fim) {
    int esq = 0, dir = 0;
    struct registro aux, pivo;
    esq = inicio;
    dir = fim;
    pivo = V[inicio];

    while (esq < dir) {
        while (V[esq].nome[0] <= pivo.nome[0]) 
            esq += 1;
        while (V[dir].nome[0] > pivo.nome[0])
            dir -= 1;
        if (esq < dir) {
            aux = V[esq];
            V[esq] = V[dir];
            V[dir] = aux;
        }
    }

    V[inicio] = V[dir];
    V[dir] = pivo;
    return dir;
}

void heapSortTamanho(struct registro *V, int N) {
    int i = 0;
    struct registro aux;

    for (i = (N - 1) / 2; i >= 0; i--) {
        criaHeapTamanho(V, i, N - 1);
    }

    for (i = N - 1; i >= 1; i--) {
        aux = V[0];
        V[0] = V[i];
        V[i] = aux;
        criaHeapTamanho(V, 0, i - 1);
    }
}

void criaHeapTamanho(struct registro *vet, int i, int f) {
    struct registro aux = vet[i];
    int j = i * 2 + 1;
    
    while (j <= f) {
        if (j < f) {
            if (vet[j].tamanho < vet[j + 1].tamanho) {
                j = j + 1;
            }
        }

        if (aux.tamanho < vet[j].tamanho) {
            vet[i] = vet[j];
            i = j;
            j = 2 * i + 1;
        } else {
            j = f + 1;
        }
    }

    vet[i] = aux;
}

void heapSortData(struct registro *V, int N) {
    int i = 0;
    struct registro aux;

    for (i = (N -1) / 2; i >= 0; i--) {
        criaHeapData(V, i, N - 1);
    }

    for (i = N - 1; i >= 1; i--) {
        aux = V[0];
        V[0] = V[i];
        V[i] = aux;
        criaHeapData(V, 0, i - 1);
    }
}

void criaHeapData(struct registro *V, int i, int f) {
    struct registro aux = V[i];
    int j = i * 2 + 1;

    while (j <= f) {
        if (j < f) {
            if (compareDataMenor(V[j].data, V[j + 1].data)) {
                j = j + 1;
            }
        }

        if (compareDataMenor(aux.data, V[j].data)) {
            V[i] = V[j];
            i = j;
            j = 2 * i + 1;
        } else {
            j = f + 1;
        }
    }

    V[i] = aux;
}

void heapSortNome(struct registro *V, int N) {
  int i = 0;
  struct registro aux;

  for (i = (N -1 ) / 2; i >= 0; i--) {
    criaHeapNome(V, i, N - 1);
  }

  for (i = N -1; i >= 1; i--) {
    aux = V[0];
    V[0] = V[i];
    V[i] = aux;
    criaHeapNome(V, 0, i - 1);
  }
}

void criaHeapNome(struct registro *V, int i, int f) {
  struct registro aux = V[i];
  int j = i * 2 + 1;

  while (j <= f) {
    if (j < f) {
      if (V[i].nome[0] < V[j + 1].nome[0]) {
        j = j + 1;
      }
    }

    if (aux.nome[0] < V[j].nome[0]) {
      V[i] = V[j];
      i = j;
      j = 2 * i + 1;
    } else {
      j = f + 1;
    }
  }

  V[i] = aux;
}

void showMenuGeral(struct registro *registros, int tamanho) {
    int menu;
    printf("------------------------------------------------------------------------------------------\n");
    printf("\t\t\t APS QUARTO SEMESTRE\n");
    printf("------------------------------------------------------------------------------------------\n\n");
    printf("1. Ordenação\n");
    printf("2. Busca\n");
    printf("3. Sair\n\n");
    printf("Selecione o que fazer: ");
    scanf("%d", &menu);
    clear();

    if (menu == 1) {
        showMenuAlgoritmos(registros, tamanho);
    } else if (menu == 2) {
        showMenuBusca(registros, tamanho);
    } else if (menu == 3) {
        exit(0);
    } else {
        showMenuGeral(registros, tamanho);
    }
}

void showMenuAlgoritmos(struct registro *registros, int tamanho) {
    int algoritmo, parametro;
    printf("------------------------------------------------------------------------------------------\n");
    printf("\t\t\t APS QUARTO SEMESTRE\n");
    printf("------------------------------------------------------------------------------------------\n\n");
    printf("1. Bubble sort\n");
    printf("2. Quick sort\n");
    printf("3. Heap sort\n");
    printf("4. Voltar\n\n");
    printf("Selecione o algoritmo de ordenação: ");
    scanf("%d", &algoritmo);
    clear();

    if (algoritmo == 4) {
        showMenuGeral(registros, tamanho);
        exit(0);
    } else if (algoritmo != 1 && algoritmo != 2 && algoritmo != 3) {
        showMenuAlgoritmos(registros, tamanho);
        exit(0);
    }

    printf("------------------------------------------------------------------------------------------\n");
    printf("\t\t\t APS QUARTO SEMESTRE\n");
    printf("------------------------------------------------------------------------------------------\n\n");
    printf("1. Nome\n");
    printf("2. Data\n");
    printf("3. Tamanho\n");
    printf("4. Voltar\n\n");
    printf("Selecione o parametro de ordenação: ");
    scanf("%d", &parametro);
    clear();

    if (parametro == 4) {
        showMenuAlgoritmos(registros, tamanho);
        exit(0);
    }

    clock_t Ticks[2];
    Ticks[0] = clock();
    switch (algoritmo){
    case 1: // Bubble sort
        switch (parametro) {
        case 1: // Nome
            bubbleSortNome(registros, tamanho);
            Ticks[1] = clock();
            break;
        case 2: // Data
            bubbleSortData(registros, tamanho);
            Ticks[1] = clock();
            break;
        case 3: // Tamanho
            bubbleSortTamanho(registros, tamanho);
            Ticks[1] = clock();
            break;
        default:
            break;
        }
        break;
    case 2: // Quick sort
        switch (parametro) {
        case 1: // Nome
            quickSortNome(registros, 0, tamanho - 1);
            Ticks[1] = clock();
            break;
        case 2: // Data
            quickSortData(registros, 0, tamanho - 1);
            Ticks[1] = clock();
            break;
        case 3: // Tamanho
            quickSortTamanho(registros, 0, tamanho - 1);
            Ticks[1] = clock();
            break;
        default:
            break;
        }
        break;
    case 3: // Heap sort
        switch (parametro) {
        case 1: // Nome
            heapSortNome(registros, tamanho);
            Ticks[1] = clock();
            break;
        case 2: // Data
            heapSortData(registros, tamanho);
            Ticks[1] = clock();
            break;
        case 3: // Tamanho
            heapSortTamanho(registros, tamanho);
            Ticks[1] = clock();
            break;
        default:
            break;
        }
        break;
    default:
        break;
    }

    double tempo = (Ticks[1] - Ticks[0]) * 1000.0 / CLOCKS_PER_SEC;
    printf("Os registros foram ordenados.\n");
    printf("\nTempo gasto: %g ms.\n", tempo);
}

int comparaNome(char *nome1, char *nome2) {
    int response = 1;
    for (int i = 0; i < 100; i++) {
        if (nome1[i] != nome2[i]) {
            response = 0;
            break;
        }
    }
    return response;
}

int buscaNome(struct registro *V, char *nomeBusca, int tamanho) {
    for (int i = 0; i < tamanho; i++) {
        if (comparaNome(nomeBusca, V[i].nome)) {
            return i;
        }
    }
    return -1;
}

int buscaData(struct registro *V, struct data dataB, int comeco, int fim) {
    int meio, achou = -1;
    meio = (comeco + fim) / 2;

    if (comeco <= fim) {
        if (dataB.dia == V[meio].data.dia && dataB.mes == V[meio].data.mes && dataB.ano == V[meio].data.ano) {
            achou = meio;
            return achou;
        } else {
            if (compareDataMenor(dataB, V[meio].data)) {
                return buscaData(V, dataB, comeco, meio - 1);
            } else {
                return buscaData(V, dataB, meio + 1, fim);
            }
        }
    } else {
        return achou;
    }
}

void showLerNome(struct registro *registros, int tamanho) {
    printf("------------------------------------------------------------------------------------------\n");
    printf("\t\t\t APS QUARTO SEMESTRE\n");
    printf("------------------------------------------------------------------------------------------\n\n");
    printf("Escreva o nome para busca: ");
    getchar();
    char nomeBusca[100] = {' '};
    scanf("%[^\n]s", &nomeBusca);
    clear();
    printf("PROCURANDO REGISTRO...");
    int index = buscaNome(registros, nomeBusca, tamanho);
    clear();
    if (index > -1) {
        printRegistro(registros[index]);
    } else {
        printf("Nao foram encontrados resultados para a busca.\n");
    }
}

void showLerData(struct registro *registros, int tamanho) {
    printf("------------------------------------------------------------------------------------------\n");
    printf("\t\t\t APS QUARTO SEMESTRE\n");
    printf("------------------------------------------------------------------------------------------\n\n");
    printf("Escreva a data no formato DD/MM/AAAA: ");
    int dia, mes, ano;
    scanf("%d/%d/%d", &dia, &mes, &ano);

    struct data dataBusca;
    dataBusca.dia = dia;
    dataBusca.mes = mes;
    dataBusca.ano = ano;
    heapSortData(registros, tamanho);

    clear();
    printf("PROCURANDO REGISTRO...");
    int index = buscaData(registros, dataBusca, 0, tamanho - 1);
    clear();
    if (index > -1) {
        printRegistro(registros[index]);
    } else {
        printf("Nao foram encontrados resultados para a busca.\n");
    }
}

void showMenuBusca(struct registro *registros, int tamanho) {
    int parametro; 
    printf("------------------------------------------------------------------------------------------\n");
    printf("\t\t\t APS QUARTO SEMESTRE\n");
    printf("------------------------------------------------------------------------------------------\n\n");
    printf("1. Nome\n");
    printf("2. Data\n");
    printf("3. Voltar\n\n");
    printf("Selecione o parametro de busca: ");
    scanf("%d", &parametro);
    clear();
    if (parametro == 3) {
        showMenuGeral(registros, tamanho);
        exit(0);
    } else if (parametro < 1 || parametro > 3) {
        showMenuBusca(registros, tamanho);
        exit(0);
    }

    switch (parametro) {
    case 1: // Nome
        showLerNome(registros, tamanho);
        break;
    case 2: // Data
        showLerData(registros, tamanho);
        break;
    default:
        break;
    }
}

void lerDados(FILE *arq, struct registro *registros) {
    int num_registros = numeroRegistros(arq);
    int count_registro = 0;
    char ch = 0;

    while ((ch = fgetc(arq)) != EOF) {
        
        // Captura o nome do arquivo 
        int aux = 0;
        do {
            registros[count_registro].nome[aux] = ch;
            aux += 1;
        } while (((ch = fgetc(arq)) != ';') && ch != EOF);

        // Coloca o resto dos caracteres como espaço
        for (int i = aux; i < 100; i++) {
            registros[count_registro].nome[i] = 0;
        }

        // Captura o tamanho do arquivo
        char numero[10] = {' '};
        aux = 0;
        while (((ch = fgetc(arq)) != ';') && ch != EOF) {
            numero[aux] = ch;
            aux += 1;
        }
        registros[count_registro].tamanho = atoi(numero);

        // Captura data
        char aux_data[4] = {' '};

        aux_data[0] = fgetc(arq);
        aux_data[1] = fgetc(arq);

        registros[count_registro].data.dia = atoi(aux_data);

        // retirar o simbolo '/'
        fgetc(arq);

        aux_data[0] = fgetc(arq);
        aux_data[1] = fgetc(arq);

        registros[count_registro].data.mes = atoi(aux_data);

        // retirar o simbolo '/'
        fgetc(arq);

        for (int i = 0; i < 4; i++) {
            aux_data[i] = fgetc(arq);
        }

        for (int i = 4; i < 13; i++) {
            aux_data[i] = ' ';
        }

        registros[count_registro].data.ano = atoi(aux_data);

        // limpar os espaços que não vão ser usados
        aux_data[2] = ' ';
        aux_data[3] = ' ';

        // retirar o simbolo espaco que separa a data e a hora
        fgetc(arq);

        // Captura hora
        aux_data[0] = fgetc(arq);
        aux_data[1] = fgetc(arq);

        registros[count_registro].horario.hora = atoi(aux_data);

        // retirar o simbolo ':'
        fgetc(arq);
        
        aux_data[0] = fgetc(arq);
        aux_data[1] = fgetc(arq);

        registros[count_registro].horario.minuto = atoi(aux_data);

        // retirar o simbolo ':'
        fgetc(arq);

        aux_data[0] = fgetc(arq);
        aux_data[1] = fgetc(arq);

        registros[count_registro].horario.segundo = atoi(aux_data);

        // retirar o proximo ';'
        fgetc(arq);

        // Captura as coordenadas
        char latitude[20] = {" "};
        aux = 0;
        while (((ch = fgetc(arq)) != ',') && ch != EOF) {
            latitude[aux] = ch;
            aux += 1;
        }
        registros[count_registro].localizacao[0] = atof(latitude);

        char longitude[20] = {" "};
        aux = 0;
        while (((ch = fgetc(arq)) != '\n') && ch != EOF) {
            longitude[aux] = ch;
            aux += 1;
        }
        registros[count_registro].localizacao[1] = atof(longitude);
        
        count_registro += 1;
    }
}

void printDados(struct registro *V, int tamanho) {
    for (int i = 0; i < tamanho; i++) {
        printf("%s;%d;%d/%d/%d %d:%d:%d;%f,%f\n",
            V[i].nome,
            V[i].tamanho,
            V[i].data.dia,
            V[i].data.mes,
            V[i].data.ano,
            V[i].horario.hora,
            V[i].horario.minuto,
            V[i].horario.segundo,
            V[i].localizacao[0],
            V[i].localizacao[1]);
    }
}

void printRegistro(struct registro dado) {
    printf("Nome: %s\n", dado.nome);
    printf("Tamanho: %d\n", dado.tamanho);
    printf("Data: %d/%d/%d\n", dado.data.dia, dado.data.mes, dado.data.ano);
    printf("Hora: %d:%d:%d\n", dado.horario.hora, dado.horario.minuto, dado.horario.segundo);
    printf("Localidade: %f,%f\n", dado.localizacao[0], dado.localizacao[1]);
}